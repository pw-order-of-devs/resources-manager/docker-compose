# Resources Manager Docker Compose

## Table of Contents

- [About](#about)
- [Environment Variables](#env)
- [Usage](#usage)

## About <a name = "about"></a>

Docker Compose configuration for Resources Manager

## Environment Variables <a name="env"></a>

Create a .env file in the root of your project

```
touch .env # create a new .env file
vim .env # open the .env file in the vim
```

## Usage <a name="usage"></a>

To start the system:

```
docker-compose up -d
```
